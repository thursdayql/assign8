
public abstract class Books
{
  protected String title;
  protected String author;
  protected int pages;

  public Books(String title, String author, int pages)
  {
    this.title = title;
    this.author = author;
    this.pages = pages;
  }

  public String toString()
  {
    String str = "";
    str += "Book Title: " + title + "\n";
    str += "Book Author: " + author + "\n";
    str += "Book Pages: " + pages + "\n";

    return str;
  }
}
