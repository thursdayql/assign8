
public class MediaLiterature
{
  protected String title;
  protected String author;
  protected int pages;
  protected String releasePeriod;

  public MediaLiterature(String title, String author, int pages, String releasePeriod)
  {
    this.title = title;
    this.author = author;
    this.pages = pages;
    this.releasePeriod = releasePeriod;
  }
  
  public String toString()
  {
    String str = "";
    str += "Literature Title: " + title + "\n";
    str += "Literature Author: " + author + "\n";
    str += "Literature Pages: " + pages + "\n";
    str += "Literature Release Period: Every " + releasePeriod + "\n";
    
    return str;
  }
}
