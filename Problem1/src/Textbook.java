
public class Textbook extends Books
{
  private String type = "Textbook";
  
  protected String subject;
  protected int grade;
  
  public Textbook(String title, String author, int pages, String subject, int grade)
  {
    super(title, author, pages);
    this.subject = subject;
    this.grade = grade;
  }
  
  public String toString()
  {
    String str = super.toString();
    str += "Educational Subject: " + subject + "\n";
    str += "Book Type: " + type + "\n";
    
    return str;
  }
  
}
