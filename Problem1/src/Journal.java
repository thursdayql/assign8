
public class Journal extends MediaLiterature
{
  private String type = "Journal";

  protected int numArticles;

  public Journal(String title, String author, int pages, String releasePeriod,
      int numArticles)
  {
    super(title, author, pages, releasePeriod);
    this.numArticles = numArticles;
  }

  public String toString()
  {
    String str = super.toString();
    str += "Number of Articles: " + numArticles + "\n";
    str += "Media Type: " + type + "\n";

    return str;
  }
}
