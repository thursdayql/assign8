
public class Magazine extends MediaLiterature
{
  private String type = "Magazine";

  protected String genre;

  public Magazine(String title, String author, int pages, String releasePeriod,
      String genre)
  {
    super(title, author, pages, releasePeriod);
    this.genre = genre;
  }

  public String toString()
  {
    String str = super.toString();
    str += "Genre: " + genre + "\n";
    str += "Media Type: " + type + "\n";

    return str;
  }

}
