
//********************************************************************
//File:	        Driver.java       
//Author:       Liam Quinn
//Date:	        November 21st, 2017
//Course:       CPS100
//
//Problem Statement:
// Define and implement a set of classes that define various types
// of reading material: books, novels, magazines, technical journals,
// textbooks, and so on. Include data values that describe various
// attributes of the material, such as the number of pages and the
// names of the primary characters. Include methods that are named 
// appropriately for each class and that print an appropriate message.
// Create a main driver class to instantiate and exercise several of the
// classes.
//
//Inputs:	None 
//Outputs:  toStrings created from each subclass
// 
//********************************************************************

public class Driver
{

  public static void main(String[] args)
  {

    Novel novel = new Novel("Bible Story", "Unknown", 1200, "David", "Goliath");
    System.out.println(novel.toString());

    Textbook textbook = new Textbook("BC Science 10", "McGraw-Hill Ryerson",
        538, "Science", 10);
    System.out.println(textbook.toString());

    Magazine magazine = new Magazine("National Geographic",
        "Alexander Graham Bell", 84, "1 Month", "Science and People");
    System.out.println(magazine.toString());

    Journal journal = new Journal("Scientific Reports", "Suzanne Farley", 471,
        "6 Months", 96);
    System.out.println(journal.toString());


  }

}
