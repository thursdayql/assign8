
public class Novel extends Books
{
  private String type = "Novel";

  protected String protagonist;
  protected String antagonist;

  public Novel(String title, String author, int pages, String protagonist,
      String antagonist)
  {
    super(title, author, pages);
    this.protagonist = protagonist;
    this.antagonist = antagonist;
  }

  public String toString()
  {
    String str = super.toString();
    str += "Protagonist: " + protagonist + "\n";
    str += "Antagonist: " + antagonist + "\n";

    str += "Book Type: " + type + "\n";

    return str;
  }
}
